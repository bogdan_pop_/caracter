import unittest
import random

class Caracter(object):
    def __init__(self):
        self.hp = 100
        self.x = 0
        self.y = 0
        self.arme = ['dagger', 'sabie', 'buzdugan']
        self.wp = False
        self.scut = False
        self.scuturi = ['mic', 'mediu', 'mare', 'special']
        self.armura = False
        self.weapons = {'dagger': 25, 'sabie': 35, 'buzdugan':50}
        self.shields = {'mic':5, 'mediu': 10, 'mare': 15, 'special': 20}


    def get_hp(self):
        return self.hp


    def moveR(self, x):
        self.x += x
        if self.x < 0 or self.x > 100:
            print 'Ai iesit afara din tarc'
            self.x -= x
        return 'Noua pozitie x: {}, y: {}'.format(self.x, self.y)


    def moveL(self, x):
        self.x -= x
        if self.x < 0 or self.x > 100:
            print 'Ai iesit afara din tarc'
            self.x += x
        return 'Noua pozitie x: {}, y: {}'.format(self.x, self.y)


    def moveD(self, y):
        self.y += y
        if self.y < 0 or self.y > 100:
            print 'Ai iesit afara din tarc'
            self.y -= y
        return 'Noua pozitie x: {}, y: {}'.format(self.x, self.y)


    def moveU(self, y ):
        self.y -= y
        if self.y < 0 or self.y > 100:
            print 'Ai iesit afara din tarc'
            self.y += y
        return 'Noua pozitie x: {}, y: {}'.format(self.x, self.y)


    def are_arma(self):
        if self.wp == False:
            return False
        else:
            return self.wp


    def are_armura(self):
        if self.armura == False:
            return False
        else:
            return True


    def are_scut(self):
        if self.scut == False:
            return False
        else:
            return True

    def is_dead(self):
        return not (self.hp > 0)


class Hero(Caracter):
    def __init__(self, name):
        Caracter.__init__(self)
        self.name = name
        print 'Initializare caracter cu numele {}, pozitie x: {}, poz y: {} si momentan esti in curu gol'.format(self.name,self.x, self.y)
        print 'Dar putem schimba asta'


    def get_name(self):
        return self.name

    def eat(self, food):
        if food == 'mar':
            self.hp += 10
            print 'Creste viata cu 10...'
        elif food == 'cremvusti':
            self.hp += 15
            print 'Creste viata cu 15. E proteina...'
        elif food == 'curcan':
            self.hp += 25
            print '+ 25 hp'
        elif food == 'soia':
            self.hp -= 50
            print 'Bleah, ce naspa ii soia'
            print '-50 hp'


    def add_wp(self, choice=None):
        if choice == None:
            self.wp = random.choice(self.arme)
            return 'You know have a {}'.format(self.wp)
        self.wp = choice
        return 'Eroul tau are acum {}'.format(choice)


    def add_scut(self, choice=False):
        if choice == False:
            self.scut = random.choice(self.scuturi)
            return 'You know have a '.format(self.scut)
        self.scut = choice
        return 'Eroul tau are acum scut {}'.format(choice)


    def add_armura(self):
        if self.armura == False:
            self.armura = True
        print 'Tocmai ai obtinut armura'


    def attack(self, other):

        #trebe arma prima data
        if self.wp == False:
            self.add_wp()
        print 'Atacking {} with {}'.format(other.get_type(), self.wp)
        other.hp -= self.weapons[self.wp]

        if other.are_scut():
            other.hp += self.shields[other.scut]

        if other.are_armura():
            other.hp += (self.weapons[self.wp] * 0.2)
        print '{}-ul mai are {} hp'.format(other.get_type(), other.get_hp())

    def special_attack(self, other):
        print 'Special attack against {} with {}'.format(other.get_type(), self.wp)
        if self.wp != 'dagger':
            other.hp -= self.weapons[self.wp] - 10
        else:
            other.hp -= self.weapons[self.wp]
        print '{}-ul mai are {} hp'.format(other.get_type(), other.get_hp())


class Enemy(Caracter):
    def __init__(self):
        Caracter.__init__(self)


    def get_type(self):
        return self.type


    def attack(self, other):
        print 'Attacking {} with {}.'.format(other.get_name(), self.wp)
        #fara scut si fara armura
        if other.are_scut() == False and other.armura == False:
            other.hp -= self.weapons[self.wp]

        #am de toate
        elif other.are_scut() == True and other.armura == True:
            other.hp -= self.weapons[self.wp]
            other.hp += self.shields[other.scut]
            other.hp += (self.weapons[self.wp] * 0.2)

        #am scut, dar nu am armura
        elif other.are_scut() == True and other.armura == False:
            other.hp -= self.weapons[self.wp] - self.shields[other.scut]

        #nu am scut, dar am armura
        elif other.are_scut() == False and other.armura == True:
            other.hp -= (self.weapons[self.wp] * 0.2)

        print 'Eroul tau {} mai are {} hp'.format(other.get_name(), other.get_hp())


class Grunt(Enemy):
    def __init__(self):
        Enemy.__init__(self)
        self.scut = 'mic'
        self.wp = 'dagger'
        self.armura = False
        self.type = 'Grunt'
        self.x = 10
        self.y = 10
        print 'Initializare {} cu scut {}, cu {} si fara armura, la pozitia x: {} si pozitia y: {}'.format(self.type, self.scut, self.wp, self.x, self.y)


class Captain(Enemy):
    def __init__(self):
        Enemy.__init__(self)
        self.type = 'Captain'
        self.scut = 'mediu'
        self.wp = 'sabie'
        self.armura = False
        self.x = 25
        self.y = 25
        print 'Initializare {} cu scut {}, cu {} si fara armura, la pozitia x: {} si pozitia y: {}'.format(self.type, self.scut, self.wp, self.x, self.y)


class MiniBoss(Enemy):
    def __init__(self):
        Enemy.__init__(self)
        self.type = 'MiniBoss'
        self.scut = 'mare'
        self.wp = 'buzdugan'
        self.armura = True
        self.x = 40
        self.y = 40
        print 'Initializare {} cu scut {}, cu {} si cu armura, la pozitia x: {} si pozitia y: {}'.format(self.type, self.scut, self.wp, self.x, self.y)

class Boss(Enemy):
    def __init__(self):
        Enemy.__init__(self)
        self.type = 'Boss'
        self.scut = 'special'
        self.wp = 'buzdugan'
        self.armura = True
        self.x = 50
        self.y = 50
        print 'Initializare {} cu scut {}, cu {} si cu armura, la pozitia x: {} si pozitia y: {}'.format(self.type, self.scut, self.wp, self.x, self.y)



def gameLoop():
    # def choose_enemy():
    b = Boss()
    g = Grunt()
    mb = MiniBoss()
    c = Captain()
    l = [b, g, mb, c]
    inamic = random.choice(l)


    h = Hero('Bogdan')
    h.add_scut()
    h.add_armura()
    h.add_wp()

    while True:
        if inamic.type == 'MiniBoss' or inamic.type == 'Boss':
            h.special_attack(inamic)
        else:
            h.attack(inamic)
        if inamic.is_dead():
            print 'Ai castigat, {}-ul a murit'.format(inamic.get_type())
            break
        inamic.attack(h)
        if h.is_dead():
            print 'Tragedie, marele tau erou {}, a decedat.'.format(h.get_name())
            break


class MyTest(unittest.TestCase):
    def testGetHpMethod(self):
        c = Caracter()
        c.get_hp()
        self.assertEquals(c.hp, 100)


    def testMovement(self):
        c = Caracter()
        c.moveR(115)
        self.assertEquals(c.x, 0)
        c.moveL(115)
        self.assertEquals(c.x, 0)
        c.moveD(115)
        self.assertEquals(c.y, 0)
        c.moveU(115)
        self.assertEquals(c.y, 0)
        c.moveR(50)
        self.assertEquals(c.x, 50)
        c.moveL(45)
        self.assertEquals(c.x, 5)
        c.moveD(50)
        self.assertEquals(c.y, 50)
        c.moveU(45)
        self.assertEquals(c.y, 5)


    def testHeroConstructor(self):
        erou = Hero('Bogdan')
        self.assertEquals(erou.name, 'Bogdan')


    def testHeroMovement(self):
        erou = Hero('Bogdan')
        erou.moveR(15)
        self.assertEquals(erou.x, 15)
        erou.moveL(20)
        self.assertEquals(erou.x, 15)
        erou.moveD(40)
        self.assertEquals(erou.y, 40)
        erou.moveU(50)
        self.assertEquals(erou.y, 40)


    def testHeroEatMethod(self):
        erou = Hero('Bogdan')
        erou.eat('mar')
        self.assertEquals(erou.hp, 110)
        erou.eat('cremvusti')
        self.assertEquals(erou.hp, 125)
        erou.eat('curcan')
        self.assertEquals(erou.hp, 150)
        erou.eat('soia')
        self.assertEquals(erou.hp, 100)


    def testHeroGetMethods(self):
        erou = Hero('Bogdan')
        erou.get_hp()
        self.assertEquals(erou.hp, 100)
        erou.are_armura()
        self.assertEquals(erou.armura, False)
        erou.are_scut()
        self.assertEquals(erou.scut, False)
        erou.add_scut()
        self.assertIsNotNone(erou.scut)
        erou.add_armura()
        self.assertEquals(erou.armura, True)
        erou.are_arma()
        self.assertEquals(erou.wp, False)
        erou.add_wp()
        self.assertIsNotNone(erou.wp)


    def testGruntConstructor(self):
        g = Grunt()


    def testGruntMoveMethods(self):
        g = Grunt()
        g.get_hp()
        self.assertEquals(g.hp, 100)

        g.moveU(50)
        self.assertEquals(g.y, 10)
        g.moveD(101)
        self.assertEquals(g.y, 10)
        g.moveR(101)
        self.assertEquals(g.x, 10)
        g.moveL(10)
        self.assertEquals(g.x, 0)


    def testGruntGetMethods(self):
        g = Grunt()
        g.get_type()
        self.assertEquals(g.type, 'Grunt')
        g.get_hp()
        self.assertEquals(g.hp, 100)
        g.are_armura()
        self.assertEquals(g.armura, False)
        g.are_scut()
        self.assertIsNotNone(g.scut)
        g.are_arma()
        self.assertIsNotNone(g.wp)


    def testCaptainGetMethods(self):
        c = Captain()
        c.get_type()
        self.assertEquals(c.type, 'Captain')
        c.get_hp()
        self.assertEquals(c.hp, 100)
        c.are_armura()
        self.assertEquals(c.armura, False)
        c.are_scut()
        self.assertIsNotNone(c.scut)
        c.are_arma()
        self.assertIsNotNone(c.wp)


    def testBossGetMethods(self):
        mb = MiniBoss()
        mb.get_type()
        self.assertEquals(mb.type, 'MiniBoss')
        mb.get_hp()
        self.assertEquals(mb.hp, 100)
        mb.are_armura()
        self.assertEquals(mb.armura, True)
        mb.are_scut()
        self.assertIsNotNone(mb.scut)
        mb.are_arma()
        self.assertIsNotNone(mb.wp)


    def testBossGetMethods(self):
        b = Boss()
        b.get_type()
        self.assertEquals(b.type, 'Boss')
        b.get_hp()
        self.assertEquals(b.hp, 100)
        b.are_armura()
        self.assertEquals(b.armura, True)
        b.are_scut()
        self.assertIsNotNone(b.scut)
        b.are_arma()
        self.assertIsNotNone(b.wp)

    def testHeroAttackMethodSimplu(self):
        h = Hero('Bogdan')
        g = Grunt()
        c = Captain()
        b = Boss()
        h.attack(g)
        self.assertIsNot(g.hp, 100)
        g.attack(h)
        self.assertIsNot(h.hp, 100)
        h.attack(c)
        self.assertIsNot(c.hp ,100)
        h.attack(b)
        self.assertIsNot(b.hp, 100)
        c.attack(h)
        self.assertIsNot(h.hp, 100)
        b.attack(h)
        self.assertIsNot(h.hp, 100)

    def testHeroAttackMethodComplex(self):
        h = Hero('Bogdan')
        g = Grunt()
        h.attack(g)
        if h.wp == 'dagger':
            self.assertEquals(g.hp, 80)
        elif h.wp == 'sabie':
            self.assertEquals(g.hp, 70)
        elif h.wp == 'buzdugan':
            self.assertEquals(g.hp, 55)

        c = Captain()
        h.attack(c)
        if h.wp == 'dagger':
            self.assertEquals(c.hp, 85)
        elif h.wp == 'sabie':
            self.assertEquals(c.hp, 75)
        elif h.wp == 'buzdugan':
            self.assertEquals(c.hp, 60)

        mb = MiniBoss()
        h.attack(mb)
        if h.wp == 'dagger':
            self.assertEquals(mb.hp, 95)
        elif h.wp == 'sabie':
            self.assertEquals(mb.hp, 87)
        elif h.wp == 'buzdugan':
            self.assertEquals(mb.hp, 75)

        b = Boss()
        h.attack(b)
        if h.wp == 'dagger':
            self.assertEquals(b.hp, 100)
        elif h.wp == 'sabie':
            self.assertEquals(b.hp, 92)
        elif h.wp == 'buzdugan':
            self.assertEquals(b.hp, 80)

    def testHeroSpecialAttack(self):
        h = Hero('Bogdan')
        h.add_wp('sabie')
        mb = MiniBoss()
        h.special_attack(mb)
        self.assertEquals(mb.hp, 75)
        b = Boss()
        h.add_wp('buzdugan')
        h.special_attack(b)
        self.assertEquals(b.hp, 60)
        h.special_attack(b)
        self.assertEquals(b.hp, 20)


    def testGruntAttacksHeroNoShieldOrArmor(self):
        h = Hero('Bogdan')
        g = Grunt()
        g.attack(h)
        self.assertEquals(h.hp, 75)


    def testCaptainAttackHeroNoShieldOrArmor(self):
        h = Hero('Bogdan')
        c = Captain()
        c.attack(h)
        self.assertEquals(h.hp, 65)


    def testMiniBossAttackHeroNoShieldOrArmor(self):
        h = Hero('Bogdan')
        mb = MiniBoss()
        mb.attack(h)
        self.assertEquals(h.hp, 50)


    def testBossAttackHeroNoShieldOrArmor(self):
        h = Hero('Bogdan')
        b = Boss()
        b.attack(h)
        self.assertEquals(h.hp, 50)


    def testGruntAttacksHeroWIthSmallShieldNoArmor(self):
        h = Hero('Bogdan')
        h.add_scut('mic')
        g = Grunt()
        g.attack(h)
        self.assertEquals(h.hp, 80)


    def testCaptainAttacksHeroWIthSmallShieldNoArmor(self):
        h = Hero('Bogdan')
        h.add_scut('mic')
        c = Captain()
        c.attack(h)
        self.assertEquals(h.hp, 70)


    def testMiniBossAttacksHeroWIthSmallShieldNoArmor(self):
        h = Hero('Bogdan')
        h.add_scut('mic')
        mb = MiniBoss()
        mb.attack(h)
        self.assertEquals(h.hp, 55)


    def testBossAttacksHeroWIthSmallShieldNoArmor(self):
        h = Hero('Bogdan')
        h.add_scut('mic')
        b = Boss()
        b.attack(h)
        self.assertEquals(h.hp, 55)


    def testGruntAttackHeroWithMediumSheild(self):
        h = Hero('Bogdan')
        h.add_scut('mediu')
        g = Grunt()
        g.attack(h)
        self.assertEquals(h.hp, 85)


    def testCaptainAttackHeroWithMediumSheild(self):
        h = Hero('Bogdan')
        h.add_scut('mediu')
        c = Captain()
        c.attack(h)
        self.assertEquals(h.hp, 75)


    def testMiniBossAttackHeroWithMediumSheild(self):
        h = Hero('Bogdan')
        h.add_scut('mediu')
        mb = MiniBoss()
        mb.attack(h)
        self.assertEquals(h.hp, 60)

    def testBossAttackHeroWithMediumSheild(self):
        h = Hero('Bogdan')
        h.add_scut('mediu')
        b = Boss()
        b.attack(h)
        self.assertEquals(h.hp, 60)

    def testGruntAttackHeroWithLargeShield(self):
        h = Hero('Bogdan')
        h.add_scut('mare')
        g = Grunt()
        g.attack(h)
        self.assertEquals(h.hp, 90)


    def testCaptainAttackHeroWithLargeShield(self):
        h = Hero('Bogdan')
        h.add_scut('mare')
        c = Captain()
        c.attack(h)
        self.assertEquals(h.hp, 80)

    def testMiniBossAttackHeroWithLargeShield(self):
        h = Hero('Bogdan')
        h.add_scut('mare')
        mb = MiniBoss()
        mb.attack(h)
        self.assertEquals(h.hp, 65)


    def testBossAttackHeroWithLargeShield(self):
        h = Hero('Bogdan')
        h.add_scut('mare')
        b = Boss()
        b.attack(h)
        self.assertEquals(h.hp, 65)


    def testGruntAttackHeroWithSpecialShield(self):
        h = Hero('Bogdan')
        h.add_scut('special')
        g = Grunt()
        g.attack(h)
        self.assertEquals(h.hp, 95)


    def testCaptainAttackHeroWithSpecialShield(self):
        h = Hero('Bogdan')
        h.add_scut('special')
        c = Captain()
        c.attack(h)
        self.assertEquals(h.hp, 85)


    def testMiniBossAttackHeroWithSpecialShield(self):
        h = Hero('Bogdan')
        h.add_scut('special')
        mb = MiniBoss()
        mb.attack(h)
        self.assertEquals(h.hp, 70)


    def testBossAttackHeroWithSpecialShield(self):
        h = Hero('Bogdan')
        h.add_scut('special')
        b = Boss()
        b.attack(h)
        self.assertEquals(h.hp, 70)


    def testGruntAttackHeroWithNoSheildWithArmor(self):
        h = Hero('Bogdan')
        h.add_armura()
        g = Grunt()
        g.attack(h)
        self.assertEquals(h.hp, 95)


    def testCaptainAttackHeroWithNoSheildWithArmor(self):
        h = Hero('Bogdan')
        h.add_armura()
        c = Captain()
        c.attack(h)
        self.assertEquals(h.hp, 93)


    def testBossAttackHeroWithNoSheildWithArmor(self):
        h = Hero('Bogdan')
        h.add_armura()
        b = Boss()
        b.attack(h)
        self.assertEquals(h.hp, 90)


    def testMiniBossAttackHeroWithNoSheildWithArmor(self):
        h = Hero('Bogdan')
        h.add_armura()
        mb = MiniBoss()
        mb.attack(h)
        self.assertEquals(h.hp, 90)


    def testGruntAttackHeroWithSheildAndArmor(self):
        h = Hero('Bogdan')
        h.add_scut('mic')
        h.add_armura()
        g = Grunt()
        g.attack(h)
        self.assertEquals(h.hp, 85)


    def testCaptainAttackHeroWithSheildAndArmor(self):
        h = Hero('Bogdan')
        h.add_scut('mic')
        h.add_armura()
        c = Captain()
        c.attack(h)
        self.assertEquals(h.hp, 77)


    def testMiniBossAttackHeroWithSheildAndArmor(self):
        h = Hero('Bogdan')
        h.add_scut('mic')
        h.add_armura()
        mb = MiniBoss()
        mb.attack(h)
        self.assertEquals(h.hp, 65)


    def testBossAttackHeroWithSheildAndArmor(self):
        h = Hero('Bogdan')
        h.add_scut('mic')
        h.add_armura()
        b = Boss()
        b.attack(h)
        self.assertEquals(h.hp, 65)


    def testGruntAttackHeroWithSheildAndArmor(self):
        h = Hero('Bogdan')
        h.add_scut('mediu')
        h.add_armura()
        g = Grunt()
        g.attack(h)
        self.assertEquals(h.hp, 90)


    def testCaptainAttackHeroWithSheildAndArmor(self):
        h = Hero('Bogdan')
        h.add_scut('mediu')
        h.add_armura()
        c = Captain()
        c.attack(h)
        self.assertEquals(h.hp, 82)


    def testMiniBossAttackHeroWithSheildAndArmor(self):
        h = Hero('Bogdan')
        h.add_scut('mediu')
        h.add_armura()
        mb = MiniBoss()
        mb.attack(h)
        self.assertEquals(h.hp, 70)


    def testMiniBossAttackHeroWithSheildAndArmor(self):
        h = Hero('Bogdan')
        h.add_scut('mediu')
        h.add_armura()
        b = Boss()
        b.attack(h)
        self.assertEquals(h.hp, 70)


    def testGruntAttackHeroWithSheildAndArmor(self):
        h = Hero('Bogdan')
        h.add_scut('mare')
        h.add_armura()
        g = Grunt()
        g.attack(h)
        self.assertEquals(h.hp, 95)


    def testCaptainAttackHeroWithSheildAndArmor(self):
        h = Hero('Bogdan')
        h.add_scut('mare')
        h.add_armura()
        c = Captain()
        c.attack(h)
        self.assertEquals(h.hp, 87)


    def testMiniBossAttackHeroWithSheildAndArmor(self):
        h = Hero('Bogdan')
        h.add_scut('mare')
        h.add_armura()
        mb = MiniBoss()
        mb.attack(h)
        self.assertEquals(h.hp, 75)


    def testBossAttackHeroWithSheildAndArmor(self):
        h = Hero('Bogdan')
        h.add_scut('mare')
        h.add_armura()
        b = Boss()
        b.attack(h)
        self.assertEquals(h.hp, 75)


    def testHeroIsDead(self):
        h = Hero('Bogdan')
        self.assertFalse(h.is_dead())
        b = Boss()
        b.attack(h)
        b.attack(h)
        self.assertEquals(h.hp, 0)
        self.assertTrue(h.is_dead())


    def testEnemyisDead(self):
        h = Hero('Bogdan')
        h.add_wp('buzdugan')
        g = Grunt()
        h.attack(g)
        h.attack(g)
        self.assertEquals(g.hp, 10)
        h.attack(g)
        self.assertTrue(g.is_dead())

















